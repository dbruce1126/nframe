<?php

namespace MyApp\Terminators;

/**
 * Renders the login errors received from the validation step.
 */
class ShowLoginErrorsTerminator extends AbstractTerminator
{
    /**
     * Terminates the route by rendering a Twig view with the errors.
     * @param array $errors The login error messages.
     * @return string Returns the rendered Twig view as a string.
     */
    public function terminate($errors)
    {
        $view = $this->retrieve('global.twig')->loadTemplate('login.twig');
        return $view->render(['errors' => $errors]);
    }
}