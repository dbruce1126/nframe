<?php

namespace MyApp\Terminators;

/**
 * Redirects the user to the root page on successful login.
 */
class RedirectSuccessfulLoginTerminator extends AbstractTerminator
{
    /**
     * Redirects the user to the front page on successful login.
     */
    public function terminate()
    {
        header('location: /');
    }
}