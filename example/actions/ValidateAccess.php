<?php

namespace MyApp\Actions;

/**
 * Validates the user in question has access to the resource in question.
 */
class ValidateAccess extends AbstractAction
{
    /**
     * Validates that the authorized user has access rights to the resource.
     * @param string $resource The resource type.
     * @param User $user The authenticated user.
     * @param string $resourceId (optional) The optional resource ID if present.
     * @return bool Returns true if the token is valid and maps to a user and
     *         false otherwise.
     */
    public function perform($resource, User $user, $resourceId = null)
    {
        switch ($resource) {
            case 'user':
                // restrict the 'user' resource to only the same user as authentication
                if ($resourceId !== null && intval($resourceId) === $user->getUserId()) {
                    return true;
                }
            // other resources would go here for a global ACL
            // default action DENY
            default:
                $this->emit('error', ['responseCode' => 403, 'message' => 'Not authorized.']);
                return false;
        }
    }
}
