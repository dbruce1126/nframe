<?php

namespace MyApp\Actions;

/**
 * Create a user's session.
 */
class CreateSessionAction extends AbstractAction
{
    /**
     * Starts a user session.
     * @param User $user The user to create the session against.
     * @return bool Always returns true.
     */
    public function perform(User $user)
    {
        session_start();
        session_regenerate_id();
        $_SESSION['user_id'] = $user->getUserId();
        return true;
    }
}