<?php

namespace MyApp\Actions;

/**
 * Returns the user data from the database associated with the given user ID
 */
class RetrieveUserData extends AbstractAction
{
    /**
     * Emits the retrieved user from the database.
     * @param string $userId The user ID.
     * @return bool Returns true.
     */
    public function perform($userId)
    {
        $user = $this->retrieve('global.database')->getUserById($userId);
        $this->emit('user', $user);
        return true;
    }
}
