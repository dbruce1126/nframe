<?php

namespace MyApp\Actions;

/**
 * An action to validate a user's username and password like in a login form.
 */
class ValidateLoginAction extends AbstractAction
{
    /**
     * Validates a username/password pair.
     * @param string $username The username to validate.
     * @param string $password The password to validate.
     * @return bool Returns true on success and false on failure.
     */
    public function perform($username, $password)
    {
        $user = $this->retrieve('global.database')->fetchUser($username);
        if (false === $user) {
            $this->emit('errorMessages', ['Invalid username.']);
            return false;
        } else if (false === password_verify($user->getPassword(), $password)) {
            $this->emit('errorMessages', ['Invalid password.']);
            return false;
        }
        $this->emit('user', $user);
        return true;
    }
}
