<?php

namespace MyApp\Actions;

/**
 * An action to ensure the OAuth token maps to an existing user.
 */
class ValidateOAuthToken extends AbstractAction
{
    /**
     * Validates the OAuth token.
     * @param string $token The OAuth token to lookup.
     * @return bool Returns true if the token is valid and maps to a user and
     *         false otherwise.
     */
    public function perform($token)
    {
        $user = $this->retrieve('global.database')->fetchUserByToken($token);
        if (false === $user) {
            $this->emit('error', ['responseCode' => 403, 'message' => 'Not authorized.']);
            return false;
        }
        $this->emit('user', $user);
        return true;
    }
}
