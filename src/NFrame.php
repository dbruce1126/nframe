<?php

namespace NFrame\NFrame;

use \Exception;

class NFrame
{

    protected $baseDir;
    protected $configDir;

    protected $routesDir;
    protected $routingInfo;

    protected $services;

    public function __construct($baseDir)
    {
        $this->configurePaths($baseDir);
    }

    protected function configurePaths($baseDir)
    {
        if (is_dir($baseDir) && is_readable($baseDir)) {
            $this->baseDir = realpath($baseDir);
        } else {
            throw new Exception('Cannot find base directory or directory is not readable.');
        }

        $this->configDir = $this->baseDir.DIRECTORY_SEPARATOR.'config';
        if (!is_dir($this->configDir) || !is_readable($this->configDir)) {
            throw new Exception('Cannot find config directory or directory is not readable.');
        }

        $this->routesDir = $this->baseDir.DIRECTORY_SEPARATOR.'routes';
        if (!is_dir($this->routesDir) || !is_readable($this->routesDir)) {
            throw new Exception('Cannot find routes directory or directory is not readable.');
        }

        $routesConfigFile = $this->configDir.DIRECTORY_SEPARATOR.'routes.php';
        if (is_readable($routesConfigFile)) {
            $this->routingInfo = require $routesConfigFile;
        }

        if (!is_array($this->routingInfo)) {
            throw new Exception('Cannot find route config file or file does not return an array.');
        }

    }

    public function dispatch()
    {
        $dispatcher = \FastRoute\simpleDispatcher(function(\FastRoute\RouteCollector $collector) {
            return $this->registerRoutes($collector);
        });
        $routeInfo = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
        switch ($routeInfo[0]) {
            case \FastRoute\Dispatcher::NOT_FOUND :
                http_response_code(404);
                return '404 Page not found.';
            case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
                header('Allow: '.implode(',', $routeInfo[1]));
                http_response_code(405);
                return '405 Method not allowed.';
        }

        return 'hello world';
    }

    protected function registerRoutes(\FastRoute\RouteCollector $collector)
    {
        $allVerbs = ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'HEAD'];
        $verbPattern = sprintf('/^(%s) (.+)$/', implode('|', $allVerbs));
        foreach ($this->routingInfo as $path => $routeFile) {
            if (preg_match($verbPattern, $path, $matches)) {
                $collector->addRoute($matches[1], $matches[2], $routeFile);
            } else {
                foreach ($allVerbs as $verb) {
                    $collector->addRoute($verb, $path, $routeFile);
                }
            }
        }
    }

    public static function handleRequest($baseDir)
    {
        $instance = new self($baseDir);
        echo $instance->dispatch();
    }
}
