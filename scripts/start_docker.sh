#!/bin/bash
docker run -d --name gearman nframe/gearman
docker run -d --name php -v `pwd`:/srv --link gearman:gearman nframe/php-fpm
docker run -d --name web -v `pwd`:/var/www/html --link php:php -p 80:80 nframe/nginx
